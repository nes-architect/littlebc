let Express = require("express");
let BodyParser = require("body-parser");
let fs = require("fs");
let HTTP_PORT = require("../config").http_port;

let BlockChain = require("./Blockchain");
let Block = require("./obj/Block");
let ServerP2P = require("./ServerP2P");

let ServerHTTP = function(){

    console.log("[MAIN] [INF] Starting HTTP server...");
    let app = Express();
    app.use(BodyParser.json());
    app.use(BodyParser.urlencoded({extended: true}));

    /**
     * List all the blocks
     */
    app.get("/block", function(req,res){

        let files = fs.readdirSync("./datas/").sort((a,b)=>(a.split(".blk")[0]-b.split(".blk")[0]));
        let blocks = [];
        for(let i=0; i < files.length; i++){
            blocks.push(Block.fromFILE(files[i]))
        }

        res.json(blocks);
    });

    /**
     * Get a specified block
     */
    app.get("/block/:index", function(req,res){
        let files = fs.readdirSync("./datas/").sort((a,b)=>(a.split(".blk")[0]-b.split(".blk")[0]));

        if(req.params.index > files.length-1){
            res.json("This block did not exists yet, the latest block is #"+(files.length-1));
        }
        else{
            res.json(Block.fromFILE(files[req.params.index]));
        }

    });

    /**
     * Create a new block and add it to the chain
     */
    app.post("/block", function(req,res){

        let newBlock = BlockChain.generateNextBlock(req.body.datablock);
        if(!newBlock){
            res.json({success:false,data: "Can't add new block because the local chain is not valid yet"});
        }
        else{
            BlockChain.addNewBlock(newBlock);
            console.log("[MAIN] [INF] New block added in the local chain ! #",newBlock.index);
            // Send a message on the network to say 'I've a new block !!'
            ServerP2P.broadcast(ServerP2P.Message_responseLatestBlock());

            res.json({"success":true,"datas":newBlock});
        }

    });

    /**
     * list all peers connected to this nodes
     */
    app.get("/peers", function(req,res){
       res.json("peers");
    });

    app.post("/peers", function(req,res){
       res.json("Add new peers !");
    });

    /**
     * Start the listening of the Server
     */
    app.listen(HTTP_PORT, function(){
        console.log("[MAIN] [OK] HTTP Server listening on", HTTP_PORT);
    });


}

module.exports = ServerHTTP;