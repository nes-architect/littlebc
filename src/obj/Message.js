/**
 * Constructor of a Message packet
 * @param type
 * @constructor
 */
let Message = function(){

}

/**
 * Define types of message can be sended between peers
 * @type {{QUERY_LATEST: number, QUERY_ALL: number, RESPONSE_BLOCKCHAIN: number}}
 */
Message.MESSAGES_TYPE = {
    QUERY_LATEST:        0,     // When a PEER ask for the lastest block into the local blockchain
    QUERY_ALL:           1,     // When a PEER ask for the entire blokchain
    QUERY_BLOCK:         2,
    QUERY_RANDOM_BLOCK:  3,
    RESPONSE_BLOCKCHAIN: 4,      // ??? : TBD

    SHARE_PEER_LIST:     5      // Return to the network a list of connected peers
}

/**
 * Export Message definition
 * @type {Message}
 */
module.exports = Message;