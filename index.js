console.log(
    "+-----------------------------------------+\n" +
    "| Little BlockCHain                       |\n" +
    "+-----------------------------------------+\n" +
    "| It's a Proof Of concept                 |\n" +
    "| by Thomas DEPRETZ                       |\n" +
    "+-----------------------------------------+\n"
)

// Load all environment
let BlockChain = require("./src/Blockchain");

// Load the genesis Block
BlockChain.getGenesisBlock();

// Verify the local chain
if(!BlockChain.isChainValid()){
    console.log("[MAIN] [KO] Local chain not valid");
    process.exit(1);
}else{
    console.log("[MAIN] [OK] Local chain is valid, latest local BLOCK :",BlockChain.getLatestBlock().index);
}

// Connect to each know peer
BlockChain.connectToPeers();

// Load the HTTP Server side
require("./src/ServerHTTP")();

// Load the P2P Server
require("./src/ServerP2P")();

// Every minute, check tha validity of the Chain
setInterval(function(){
    console.log("[MAIN] [INF] Regular validity check...");
    if(BlockChain.isChainValid()){
        console.log("[MAIN] [INF] Local Chain valid");
    }
    else{
        console.log("[MAIN] [KO] Local Chain corrupted x_x ! exiting...");
        process.exit(1);
    }
},60000);

/**
 * Create a test valid block
 * @type {Block}
 */
/**let Block = require("./src/obj/Block");
let block = new Block(
    3,
    "b02e03943a08ad287c42af3c9a898a4ce4c31ef007b5e77ecb5eb6e7ea1878a7",
    "1510760376197",
    "My new block !!! :D it's the second block in the time and in the blockchain",
    "cc05672373fe5b7365f9eeac39645b03a0f3c80d4e2195b576d600ecc982df1a"
);**/

//BlockChain.addNewBlock(BlockChain.generateNextBlock("Coucou ceci est le 2ème block !!!!"));
